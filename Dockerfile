FROM docker.io/python:3-slim-buster

# LDAP Exporter image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive

LABEL io.k8s.description="LDAP Prometheus Exporter Image." \
      io.k8s.display-name="LDAP Prometheus Exporter" \
      io.openshift.expose-services="9113:http" \
      io.openshift.tags="prometheus,exporter,ldap" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-ldapexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0.1"

RUN mkdir -p /usr/src/app /config
WORKDIR /usr/src/app
COPY config/* /usr/src/app/

RUN set -x \
    && mv run-exporter.sh / \
    && apt-get update \
    && apt-get install -y dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && apt-get install -y gcc \
    && chown -R 1001:root /config \
    && chmod -R g=u /config \
    && pip install --no-cache-dir -r requirements.txt \
    && apt-get remove --purge -y gcc \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENTRYPOINT [ "dumb-init", "--", "/run-exporter.sh" ]
USER 1001
